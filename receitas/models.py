from django.db import models
from django.conf import settings

# Create your models here.

class Post(models.Model):
    title_post = models.CharField(max_length=255)
    data_post = models.DateTimeField()
    desc_post = models.CharField(max_length=1000, null=True)
    conteudo = models.CharField(max_length=10000)

    def __str__(self):
        return f'{self.title_post} ({self.data_post}) - {self.conteudo}'

class Comentario(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    data_post = models.DateTimeField()
    text = models.CharField(max_length=255)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name_cat = models.CharField(max_length=255)
    desc_cat = models.CharField(max_length=255)
    post = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name_cat}'