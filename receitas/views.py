from django.shortcuts import render,  get_object_or_404
from .models import Post
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import Post, Comentario, Category
from .forms import ComentForm
from django.views import generic
from django.urls import reverse_lazy

import receitas

# Create your views here.
class list_receita(generic.ListView):
    model = Post
    template_name = 'receitas/index.html'
    def get_queryset(self):
        recei=Post.objects.all()
        context = {'titulo':"Todas as receitas",'posts': recei}
        return context

class list_cat(generic.ListView):
    model = Category
    template_name = 'receitas/categorias.html'

class detail_cat(generic.ListView):
    template_name = 'receitas/index.html'
    def get_queryset(self):
        cate = get_object_or_404(Category, pk=self.kwargs['pk'])
        recei=cate.post.all()
        context = {'titulo':cate.name_cat,'posts': recei}
        return context

def detail_receita(request, receita_id):
    receita = get_object_or_404(Post, pk=receita_id)
    context = {'Post': receita}
    return render(request, 'receitas/detail.html', context)

def create_receita(request):
    if request.method == 'POST':
        post_title = request.POST['title']
        post_data = request.POST['data']
        post_desc = request.POST['desc']
        post_conteudo = request.POST['conteudo']
        rece = Post(title_post=post_title,
                      data_post=post_data,
                      desc_post=post_desc,
                      conteudo=post_conteudo)
        rece.save()
        return HttpResponseRedirect(
            reverse('receitas:detail', args=(rece.id, )))
    else:
        return render(request, 'receitas/create.html', {})

def create_comentario(request, receita_id):
    rece = get_object_or_404(Post, pk=receita_id)
    if request.method == 'POST':
        form = ComentForm(request.POST)
        if form.is_valid():
            comen_author = form.cleaned_data['author']
            comen_data = form.cleaned_data['data_post']
            comen_coment= form.cleaned_data['text']
            comen = Comentario(author=comen_author,
                            data_post=comen_data,
                            text=comen_coment,
                            post=rece)
            comen.save()
            return HttpResponseRedirect(
                reverse('receitas:detail', args=(receita_id, )))
    else:
       form = ComentForm()
    context = {'form': form, 'receita': rece}
    return render(request, 'receitas/comentar.html', context)

def update_receita(request, receita_id):
    receita = get_object_or_404(Post, pk=receita_id)

    if request.method == "POST":
        receita.title_post = request.POST['title']
        receita.data_post = request.POST['data']
        receita.desc_post = request.POST['desc']
        receita.conteudo = request.POST['conteudo']
        receita.save()
        return HttpResponseRedirect(
            reverse('receitas:detail', args=(receita.id, )))

    context = {'receita': receita}
    return render(request, 'receitas/update.html', context)

class delete_receita(generic.DeleteView):
    model = Post
    context_object_name = 'Post'
    template_name = 'receitas/delete.html'
    success_url = reverse_lazy('receitas:index')