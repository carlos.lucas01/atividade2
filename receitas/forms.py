from django.forms import ModelForm
from .models import Post,Comentario


class ComentForm(ModelForm):
    class Meta:
        model = Comentario
        fields = [
            'author',
            'data_post',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'data_post': 'Data de postagem',
            'text': 'Comentario',
        }