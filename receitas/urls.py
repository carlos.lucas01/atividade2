from django.urls import path

from . import views

app_name = 'receitas'
urlpatterns = [
    path('', views.list_receita.as_view(), name='index'),
    path('categories/', views.list_cat.as_view(), name='categorias'),
    path('categories/<int:pk>/', views.detail_cat.as_view(), name='cat_indiv'),
    path('create/', views.create_receita, name='create'),
    path('update/<int:receita_id>/', views.update_receita, name='update'),
    path('delete/<int:pk>/', views.delete_receita.as_view(), name='delete'),
    path('<int:receita_id>/', views.detail_receita, name='detail'),
    path('<int:receita_id>/comentar', views.create_comentario, name='comentar'),
]